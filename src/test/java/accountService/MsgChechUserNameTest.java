package accountService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import modules.*;

import org.junit.Test;

public class MsgChechUserNameTest {

	@Test
	public void TestMsgChechUserName () {
		Address from = new Address ();
		Address to = new Address ();
		int sessionId = 1;
		String userName = "esha";
		MsgChechUserName msg = new MsgChechUserName (from, to, sessionId, userName);
		assertNotNull(msg);
	}
	
	@Test
	public void execTest() {
		Address from = new Address ();
		Address to = new Address ();
		int sessionId = 1;
		String userName = "esha";
		MsgChechUserName msg = new MsgChechUserName (from, to, sessionId, userName);
		AccountService someAccountService = mock(AccountService.class);
		msg.exec(someAccountService);
		verify(someAccountService).checkUserName(sessionId,userName);
	}
}
