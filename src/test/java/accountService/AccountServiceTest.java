package accountService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import messageSystem.AddressSystem;
import messageSystem.MsgSystem;

import org.junit.*;

import com.mysql.jdbc.Connection;

import modules.*;

public class AccountServiceTest {
	
	private MsgSystem messageSystem;
	private AddressSystem addressSystem;

	@Before
	public void setUp() {
		this.messageSystem = mock(MsgSystem.class);
		this.addressSystem = mock(AddressSystem.class);
	}

	@Test
	public void TestAccountService ()  {
		AccountService result = new AccountService(messageSystem, addressSystem);
		assertNotNull(result);
	}
	
	@Test
	public void getConnectionTest () {
		AccountService acs = new AccountService(messageSystem, addressSystem);
		Connection newConnect = (Connection) acs.getConnection();
		assertNull(newConnect);
	}
	
	@Test
	public void getAdressTest ()  {
		AccountService someAccountServices = new AccountService(messageSystem, addressSystem);
		Address result = someAccountServices.getAddress();	
		verify (this.addressSystem).getASId();
	}
	

}
