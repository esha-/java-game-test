package accountService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import modules.Address;

import org.junit.Test;


public class MsgAddStatisticAboutUserTest {
	
	
	
	@Test
	public void TestMsgAddStatisticAboutUser () {
		Address from = new Address ();
		Address to = new Address ();
		int userId = 1;
		int  clicks = 1 ;
		boolean vin = true ;
		MsgAddStatisticAboutUser msg = new MsgAddStatisticAboutUser(from, to, userId, clicks, vin);
		assertNotNull(msg);
	}
	
	@Test
	public void execTest() {
		Address from = new Address ();
		Address to = new Address ();
		int userId = 1;
		int  clicks = 1 ;
		boolean vin = true ;
		MsgAddStatisticAboutUser msg = new MsgAddStatisticAboutUser(from, to, userId, clicks, vin);
		AccountService someAccountService = mock(AccountService.class);
		msg.exec(someAccountService);
		verify(someAccountService).addStatisticAboutUser(userId,clicks,vin);
	}
}

