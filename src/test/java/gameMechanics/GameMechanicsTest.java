package gameMechanics;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Date;

import messageSystem.AddressSystem;
import messageSystem.MsgSystem;

import org.junit.Before;
import org.junit.Test;

import frontend.MsgStopGame;
import frontend.UpdateGameState;
import resourceSystem.Resource;

public class GameMechanicsTest {
	
	private MsgSystem messageSystem;
	private AddressSystem addressSystem;
	private Resource resource;
	int sessionId ;
	int gameSessionId = 1;
	int sessionIdAdd = 2;
	int firstUserSessionId = 1;
	int firstUserId = 1;
	int secondUserSessionId = 2;
	int secondUserId = 2;

	@Before
	public void setUp() {
		this.messageSystem = mock(MsgSystem.class);
		this.addressSystem = mock(AddressSystem.class);
		this.resource = mock (Resource.class);
	}
	
	@Test
	public void TestGameMechanics () {
		GameMechanics result = new GameMechanics(messageSystem, addressSystem, resource);
		assertNotNull(result);
	}
	
	@Test
	public void startGameSessionTest () {
		int sessionID = 1;
		GameMechanics game = new GameMechanics(messageSystem, addressSystem, resource);
		verify(messageSystem).sendMessage(new MsgStopGame(addressSystem.getGMId(),
				addressSystem.getFEId(), sessionIdAdd, sessionId));
	}
	
	@Test
	public void addClickTest () {
		MsgSystem msg = mock(MsgSystem.class);
	    GameMechanics game = new GameMechanics(msg, addressSystem, resource);
	    game.startGameSession(firstUserSessionId, firstUserId, secondUserSessionId, secondUserId);
		game.addClick(sessionIdAdd, gameSessionId, sessionIdAdd);
		verify(msg, times(2)).sendMessage(any(UpdateGameState.class));
		game.gameSessionToSession.clear();
		
		
		when(resource.getTIME_FOR_GAME()).thenReturn(-1);
		GameSession gams = new GameSession(firstUserId,secondUserId, firstUserSessionId,secondUserSessionId,
				gameSessionId, resource.getTIME_FOR_GAME()) ;
		game.gameSessionToSession.put(gameSessionId, gams);
		game.addClick(sessionIdAdd, gameSessionId, sessionIdAdd);
		verify(msg, times(3)).sendMessage(any(MsgStopGame.class));
	}
	
	@Test
	public void GetGameStateTest() {
		MsgSystem msg = mock(MsgSystem.class);
	    GameMechanics game = new GameMechanics(msg, addressSystem, resource);
	    game.startGameSession(firstUserSessionId, firstUserId, secondUserSessionId, secondUserId);
	    gameSessionId = game.gameSessionIdCreator.get();
		game.GetGameState(firstUserSessionId, firstUserId, secondUserSessionId, secondUserId, gameSessionId);
		verify(msg, times(2)).sendMessage(any(UpdateGameState.class));
		game.gameSessionToSession.clear();
		
		when(resource.getTIME_FOR_GAME()).thenReturn(-1);
		GameSession gams = new GameSession(firstUserId,secondUserId, firstUserSessionId,secondUserSessionId,
				gameSessionId, resource.getTIME_FOR_GAME()) ;
		game.gameSessionToSession.put(gameSessionId, gams);
		game.GetGameState(firstUserSessionId, firstUserId, secondUserSessionId, secondUserId, gameSessionId);
		verify(msg, times(3)).sendMessage(any(MsgStopGame.class));
		
	}
		
	@Test
	public void findGameSessionByUSessionTest() {
		gameSessionId = 1;
		
		int firstUserSession = 1;
		GameMechanics game = new GameMechanics(messageSystem, addressSystem, resource);
		int res = game.findGameSessionByUSession(firstUserSession);
		assertEquals(0, res);
		
		GameSession gams = new GameSession(firstUserId,secondUserId, firstUserSessionId,secondUserSessionId,
				gameSessionId, resource.getTIME_FOR_GAME());
		game.gameSessionToSession.put(1, gams);
		res = game.findGameSessionByUSession(firstUserSession);
		assertEquals(1, res);	
		
	
		res = game.findGameSessionByUSession(gameSessionId+1);
		assertEquals(0, res);
		
	}
	

}
