package gameMechanics;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;


import modules.Address;

public class MsgAddClickTest {

	
	@Test
	public void testSettingSessionIdsAndAddresses() throws Exception {
		Address From = new Address();
		Address To = new Address();
		int sessionId = 1;
		int gameSessionId = 2;
		int sessionIdAdd = 3;
		MsgAddClick msg = new MsgAddClick (From, To, sessionIdAdd, 
				gameSessionId, sessionId);
		assertEquals(sessionId, msg.getSessionId());
		assertEquals(gameSessionId, msg.getGameSessionId());
		assertEquals(sessionIdAdd, msg.getSessionIdAdd());
		assertTrue(From.equals(msg.getFrom()));
		assertTrue(To.equals(msg.getTo()));
		
   }
	
	@Test
	public void execTest() {
		Address From = new Address();
		Address To = new Address();
		int sessionId = 1;
		int gameSessionId = 2;
		int sessionIdAdd = 3;
		MsgAddClick msg = new MsgAddClick (From, To, sessionIdAdd, 
				gameSessionId, sessionId);
		GameMechanics someGameMechanics = mock(GameMechanics.class);
		msg.exec(someGameMechanics);
		verify(someGameMechanics).addClick(sessionIdAdd, gameSessionId, sessionId);
	}
}
