package modules;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


import modules.Address;


public class AddressTest
{
	Address addressTo;
	Address addressFrom;
	
	@Before
    public void setUp() {
		addressTo = new Address();
		addressFrom = new Address();
	}
	
	@Test
	public void testCreatingAbonentIds() {
		assertEquals(addressTo.getAbonentId(), addressFrom.getAbonentId()-1);
	}

}
