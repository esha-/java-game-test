package messageSystem;

import static org.junit.Assert.*;
import modules.*;

import org.junit.Test;

public class AddressSystemTest {

	@Test
	public void TestAddressSystem () {
		AddressSystem firstAddressSystem = new AddressSystem (2);
		assertNotNull(firstAddressSystem) ;
		AddressSystem someAddressSystem = new AddressSystem (2,2);
		assertNotNull(someAddressSystem) ;
	}
	
	@Test
	public void getAsIdTest () {
		AddressSystem someAddressSystem = new AddressSystem (2);
		someAddressSystem.setCurrentASId(someAddressSystem.asIds.size());
		Address result = someAddressSystem.getASId();
		assertEquals(0,someAddressSystem.getCurrentASId()); 
		assertNotNull (result);
		someAddressSystem.setCurrentASId(0);
		result = someAddressSystem.getASId();
		assertEquals(1,someAddressSystem.getCurrentASId()); 
		assertNotNull (result);
	}
	
		@Test
	public void getASIdByIndexTest () {
		AddressSystem someAddressSystem = new AddressSystem (3);
		Address result = someAddressSystem.getASIdByIndex(1);
		assertNotNull (result);
		someAddressSystem = new AddressSystem (0);
		assertNull (someAddressSystem.getASIdByIndex(100));
		
	}
	
	@Test
	public void getTest () {
		AddressSystem someAddressSystem = new AddressSystem (2);
		assertNotNull (someAddressSystem.getFEId());
		assertNotNull (someAddressSystem.getGMId());
	}
}
