package frontend;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import messageSystem.AddressSystem;
import messageSystem.MsgSystem;

import org.eclipse.jetty.server.Request;
import org.junit.*;

import frontend.FrontEnd.RequestType;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resourceSystem.Resource;

public class FrontEndTest {

	private MsgSystem messageSystem;
	private AddressSystem addressSystem;
	private Resource resource;
	private Request baseRequest;
	private HttpServletRequest request;
	private HttpServletResponse response;
	
	@Before
	public void setUp() {
		this.messageSystem = mock(MsgSystem.class);
		this.addressSystem = mock(AddressSystem.class);
		this.resource = mock(Resource.class);
		this.baseRequest = mock (Request.class);
		this.request = mock (HttpServletRequest.class);
		this.response = mock (HttpServletResponse.class);
	}
	
	@Test
	public void TestFrontEnd ()  {
		FrontEnd frontEnd = new FrontEnd(messageSystem, addressSystem, resource);
		assertNotNull(frontEnd);
	}

	@Test
	public void  IsUserWaitTest () {
		String userName = "esha";
		FrontEnd frontEnd = new FrontEnd(messageSystem, addressSystem, resource);
		boolean wait = frontEnd.IsUserWait(userName);
		assertFalse (wait);
		
		UserSession uss = new UserSession (userName, 1); 
		frontEnd.userSessionToSession.put(1,uss) ;
		wait = frontEnd.IsUserWait(userName);
		assertTrue (wait);
		
		wait = frontEnd.IsUserWait("esha1");
		assertFalse (wait);
		
		uss.setUserId(1);
		frontEnd.userSessionToSession.put(1,uss) ;
		wait = frontEnd.IsUserWait(userName);
		assertFalse (wait);
	}
	
	@Test
	public void IsUserInMapUserSessionTest () {
		String userName = "esha1";
		FrontEnd frontEnd = new FrontEnd(messageSystem, addressSystem, resource);
		boolean wait = frontEnd.IsUserInMapUserSession(userName);
		assertFalse (wait);
		
		UserSession usess = new UserSession (userName, 10); 
		frontEnd.userSessionToSession.put(1, usess);
		wait = frontEnd.IsUserInMapUserSession(userName);
		assertTrue (wait);
		
		wait = frontEnd.IsUserInMapUserSession("esh");
		assertFalse (wait);
	}

	@Test
	public void findAnotherReadyUserTest () {
		String userName = "esha";
		int sessionId = 1;
		FrontEnd frontEnd = new FrontEnd(messageSystem, addressSystem, resource);
		UserSession userFind = frontEnd.findAnotherReadyUser(sessionId);
		assertNull (userFind);
		
		UserSession uss = new UserSession (userName, sessionId+1); 
		frontEnd.readyForGameUsers.put(1, uss);
		userFind = frontEnd.findAnotherReadyUser(sessionId);
		assertNotNull (userFind);
		
		uss.setEnemySessionId(1);
		frontEnd.readyForGameUsers.put(1, uss);
		userFind = frontEnd.findAnotherReadyUser(sessionId);
		assertNull (userFind);
		
		uss.setSessionId(1);
		frontEnd.readyForGameUsers.put(1, uss);
		userFind = frontEnd.findAnotherReadyUser(sessionId);
		assertNull (userFind);
	}
	
	@Test
	public void checkRequestTest () {
		String userName = "eshaa";
		int sessionId = 1;
		UserSession uss = new UserSession (userName, sessionId+1); 
		FrontEnd frontEnd = new FrontEnd(messageSystem, addressSystem, resource);
		String[] s = new String [10] ;
		s[0] = "1";
		Map<String,String[]> mp = new HashMap<String, String[]>();
		
		RequestType result = frontEnd.checkRequest(baseRequest, uss);
		assertEquals (RequestType.UNKNOWN, result);
		
		mp.put("u_name", s);
		s [0] = "1";
		mp.put("uid_with_name", s);
		when(baseRequest.getParameterMap()).thenReturn(mp);
		result = frontEnd.checkRequest(baseRequest, uss);
		assertEquals (RequestType.U_NAME, result);
		mp.clear();
		
		
		s[0] = "10";
		frontEnd.userSessionToSession.put(1, uss);
	    mp.put("wait_enemy", s);
		mp.put("uid_with_name", s);
		result = frontEnd.checkRequest(baseRequest, uss);
		assertEquals (RequestType.UNKNOWN, result);
		mp.clear();
		
		s[0] = "1";
		frontEnd.userSessionToSession.put(1, uss);
		mp.put("wait_enemy", s);
		mp.put("uid_with_name", s);
		result = frontEnd.checkRequest(baseRequest, uss);
		assertEquals (RequestType.GAME_START, result);
		mp.clear();
		
		frontEnd.userSessionToSession.put(1, uss);
		frontEnd.readyForGameUsers.clear();
		mp.put("wait_enemy", s);
		mp.put("uid_with_name", s);
		result = frontEnd.checkRequest(baseRequest, uss);
		assertEquals (RequestType.WAIT_ENEMY, result);
		mp.clear();
		
		frontEnd.userSessionToSession.put(1, uss);
		frontEnd.readyForGameUsers.put(1, uss);
		mp.put("wait_enemy", s);
		mp.put("uid_with_name", s);
		result = frontEnd.checkRequest(baseRequest, uss);
		assertEquals (RequestType.WAIT_ENEMY, result);
		mp.clear();
				
		s [0] = "1";
		mp.put("game_reload", s);
		mp.put("g_s_session_id", s);
		result = frontEnd.checkRequest(baseRequest, uss);
		assertEquals (RequestType.RELOAD, result);
		mp.clear();
		
		mp.put("game_click", s);
		mp.put("g_s_session_id", s);
		result = frontEnd.checkRequest(baseRequest, uss);
		assertEquals (RequestType.CLICK, result);
		mp.clear();
		
	}
	
	@Test
	public void handleTest () throws IOException, ServletException {
		String target = "";
		
		FrontEnd fronte = new FrontEnd(messageSystem, addressSystem, resource);
		when(response.getWriter()).thenAnswer(RETURNS_MOCKS);
		fronte.handle(target, baseRequest, request, response);
	    assertNotNull(response);
	    		
		String userName = "he";
		int sessionId = 1;
		Map<String,String[]> mp = new HashMap<String, String[]>();
		when(baseRequest.getParameterMap()).thenReturn(mp);
		String[] s = new String [10] ;
		s [0] = "1";
		
		mp.put("u_name", s);
		mp.put("uid_with_name", s);
		fronte.handle(target, baseRequest, request, response);
		
		UserSession uss = new UserSession (userName, sessionId); 
		fronte.userSessionToSession.put(1,uss);
		fronte.handle(target, baseRequest, request, response);
		mp.clear();
		
		mp.put("wait_enemy", s);
		mp.put("uid_with_name", s);
		fronte.handle(target, baseRequest, request, response);
		mp.clear();
		fronte.userSessionToSession.clear();
		
		mp.put("game_click", s);
		mp.put("g_s_session_id", s);
		fronte.userSessionToSession.put(1,uss);
		fronte.userSessionToSession.put(0,uss);
		fronte.userSessionToSession.put(2,uss);
		fronte.handle(target, baseRequest, request, response);
		
		uss.setStopGame(true);
		fronte.handle(target, baseRequest, request, response);
		mp.clear();
		
		mp.put("game_reload", s);
		mp.put("g_s_session_id", s);		
		fronte.handle(target, baseRequest, request, response);
		
		uss.setStopGame(true);
		fronte.handle(target, baseRequest, request, response);
		mp.clear();
	}
	
	@Test
	public void updateGameStateTest() {
		int firstUserSessionId = 1;
		int firstUserId = 1;
		int secondUserSessionId = 1;
		int secondUserId = 2;
		int numFirstUserClicks = 1;
		int numSecondUserClicks = 1;
		int gameSessionId = 1;
		int gameTime = 10000;
		
		String userName = "esha1";
		int sessionId = 2;
		UserSession us = new UserSession (userName, sessionId); 
		FrontEnd fre = new FrontEnd(messageSystem, addressSystem, resource);
		fre.userSessionToSession.put(1, us);
		fre.updateGameState(firstUserSessionId, firstUserId, secondUserSessionId,
				secondUserId, numFirstUserClicks, numSecondUserClicks, gameSessionId, gameTime);
		assertNotNull (FrontEnd.userSessionToSession);
	} 
}











