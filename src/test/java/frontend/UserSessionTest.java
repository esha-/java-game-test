package frontend;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class UserSessionTest
{
	UserSession userSession;
	@Before
	public void setUp() throws Exception
	{
		userSession = new UserSession();
	}
	
	@Test
	public void testClearingOfGameData() throws Exception
	{
		UserSession notNullUserSession = new UserSession("Vsevolod Bolshoe Gnezdo",100500);
		notNullUserSession.clearUserGameDate();
		assertEquals(userSession.getGameSessionId(),notNullUserSession.getGameSessionId());
		assertEquals(userSession.getStopGame(),notNullUserSession.getStopGame());
		assertEquals(userSession.getEnemySessionId(),notNullUserSession.getEnemySessionId());
		assertEquals(userSession.getLastUseOfSession(),notNullUserSession.getLastUseOfSession());
	}
}
