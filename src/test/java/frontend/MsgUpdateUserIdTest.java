package frontend;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import modules.Address;

import org.junit.Test;

public class MsgUpdateUserIdTest {

	@Test
	public void TestMsgStopGame () {
		Address from = new Address ();
		Address to = new Address ();
		int sessionId = 1;
		int userId = 1;
		MsgUpdateUserId msg = new MsgUpdateUserId(from, to,  sessionId, userId);
		assertNotNull(msg);
	}
	
	@Test
	public void execTest() {
		Address from = new Address ();
		Address to = new Address ();
		int sessionId = 1;
		int userId = 1;
		MsgUpdateUserId msg = new MsgUpdateUserId(from, to,  sessionId, userId);
		FrontEnd someFrontEnd = mock(FrontEnd.class);
		msg.exec(someFrontEnd);
		verify(someFrontEnd).updateUserId(sessionId, userId);
	}

}
