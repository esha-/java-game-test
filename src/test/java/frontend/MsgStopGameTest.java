package frontend;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import modules.Address;

import org.junit.Test;

public class MsgStopGameTest {

	@Test
	public void TestMsgStopGame () {
		Address from = new Address ();
		Address to = new Address ();
		int firstUserSession = 1;
		int secondUserSession = 2;
		MsgStopGame msg = new MsgStopGame(from, to, firstUserSession, secondUserSession);
		assertNotNull(msg);
	}
	
	@Test
	public void execTest() {
		Address from = new Address ();
		Address to = new Address ();
		int firstUserSession = 1;
		int secondUserSession = 2;
		MsgStopGame msg = new MsgStopGame(from, to, firstUserSession, secondUserSession);
		FrontEnd someFrontEnd = mock(FrontEnd.class);
		msg.exec(someFrontEnd);
		verify(someFrontEnd).stopGame(firstUserSession, secondUserSession);
	}
}

