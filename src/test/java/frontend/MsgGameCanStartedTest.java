package frontend;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import modules.Address;

import org.junit.Test;

public class MsgGameCanStartedTest {

	@Test
	public void TestMsgAddStatisticAboutUser () {
		Address from = new Address ();
		Address to = new Address ();
		int sessionIdFirst = 1;
		int sessionIdSecond = 2;
		int  firstUserId = 1 ;
		int  secondUserId = 2;
		int gameSession = 1;
		MsgGameCanStarted msg = new MsgGameCanStarted(from, to, sessionIdFirst, firstUserId,
				sessionIdSecond, secondUserId, gameSession );
		assertNotNull(msg);
	}
	
	@Test
	public void execTest() {
		Address from = new Address ();
		Address to = new Address ();
		int sessionIdFirst = 1;
		int sessionIdSecond = 2;
		int  firstUserId = 1 ;
		int  secondUserId = 2;
		int gameSession = 1;
		MsgGameCanStarted msg = new MsgGameCanStarted(from, to, sessionIdFirst, firstUserId,
				sessionIdSecond, secondUserId, gameSession );
		FrontEnd someFrontEnd = mock(FrontEnd.class);
		msg.exec(someFrontEnd);
		verify(someFrontEnd).gameCanStarted(sessionIdFirst, firstUserId, sessionIdSecond, secondUserId,gameSession);
	}

}
