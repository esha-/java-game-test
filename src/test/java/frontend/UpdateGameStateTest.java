package frontend;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import modules.Abonent;
import modules.Address;

import org.junit.Test;

public class UpdateGameStateTest {
	
	@Test
	public void TestUpdateGameState () {
		Address from = new Address ();
		Address to = new Address ();
		int firstUserSessionId = 1;
		int firstUserId = 1;
		int secondUserSessionId = 2;
		int secondUserId = 2;
		int numFirstUserClicks = 1;
		int numSecondUserClicks = 2;
		int gameSessionId = 1;
		long gameTime = 10;
		UpdateGameState result = new UpdateGameState (from, to, firstUserSessionId, firstUserId, secondUserSessionId, secondUserId, 
				numFirstUserClicks, numSecondUserClicks, gameSessionId, gameTime);
		assertNotNull(result);
	}
		
	@Test
	public void execTest() {
		Address from = new Address ();
		Address to = new Address ();
		int firstUserSessionId = 1;
		int firstUserId = 1;
		int secondUserSessionId = 2;
		int secondUserId = 2;
		int numFirstUserClicks = 1;
		int numSecondUserClicks = 2;
		int gameSessionId = 1;
		long gameTime = 10;
		UpdateGameState result = new UpdateGameState (from, to, firstUserSessionId, firstUserId, secondUserSessionId, secondUserId, 
				numFirstUserClicks, numSecondUserClicks, gameSessionId, gameTime);
		FrontEnd frontEnd = mock (FrontEnd.class);
		result.exec(frontEnd);
		verify (frontEnd).updateGameState(firstUserSessionId, firstUserId, secondUserSessionId,
				secondUserId, numFirstUserClicks, numSecondUserClicks, gameSessionId, gameTime );
		Abonent newAbonent = mock (FrontEnd.class);
		result.exec(newAbonent);
		verify (frontEnd).updateGameState(firstUserSessionId, firstUserId, secondUserSessionId,
				secondUserId, numFirstUserClicks, numSecondUserClicks, gameSessionId, gameTime );
		
	}
}
