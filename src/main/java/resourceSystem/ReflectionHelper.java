package resourceSystem;

import java.lang.reflect.Field;

class Resource1
{
	private  int amountOfAS;
	private  int serverPort;
	private  int TIME_FOR_GAME;
	private int timeForClear ;

	public int getAmountOfAS()
	{
		return amountOfAS;
	}

	public int getServerPort()
	{
		return serverPort;
	}

	public int getTIME_FOR_GAME()
	{
		return TIME_FOR_GAME;
	}

	public int getTimeForClear()
	{
		return timeForClear;
	}

}



public class ReflectionHelper
{
	public static Object createInstance(String className) 	throws 	IllegalAccessException,
																	InstantiationException,
																	ClassNotFoundException
	{
			System.out.println("!");
			return Class.forName(className).newInstance();
		
	}

	public static void setFieldValue(	Object object,
										String fieldName,
										String value	)  throws 	IllegalAccessException,
																	InstantiationException,
																	ClassNotFoundException,
																	NoSuchFieldException
	{
		Field field = object.getClass().getDeclaredField(fieldName);
		field.setAccessible(true);

		value = value.trim();

		if(field.getType().equals(String.class))
		{
			field.set(object,value);
		}
		else if(field.getType().equals(int.class))
		{
			field.set(object, Integer.decode(value));
		}

		field.setAccessible(false);
	}

}
