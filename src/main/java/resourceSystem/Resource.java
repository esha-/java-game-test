package resourceSystem;

public class Resource
{
	private  int amountOfAS;
	private  int serverPort;
	private  int TIME_FOR_GAME;
	private int timeForClear ;

	public int getAmountOfAS()
	{
		return amountOfAS;
	}

	public int getServerPort()
	{
		return serverPort;
	}

	public int getTIME_FOR_GAME()
	{
		return TIME_FOR_GAME;
	}

	public int getTimeForClear()
	{
		return timeForClear;
	}

}
