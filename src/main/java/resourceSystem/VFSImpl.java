package resourceSystem;

import java.io.*;

public class VFSImpl implements VFS
{
	private String root;

	public VFSImpl(String root)
	{
		this.root = root;
	}

	public boolean isExist( String path)
	{
		return (new File(path)).exists();
	}

	public boolean isDirectory(String path)
	{
		return (new File(path)).isDirectory();
	}

	public String getAbsolutePath(String file)
	{
		return (new File(file)).getAbsolutePath();
	}

	public byte[] getBytes(String file)
	{
		byte[] fileContent= new byte[4096];
		int helpVal,i=0;
		try
		{
			InputStream  is = new FileInputStream(file);
			while( (helpVal = is.read()) >= 0 )
			{
				fileContent[i] = (byte)helpVal;
				i++;
			}
			is.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return fileContent;
	}

	public String getUFT8Text(String file)
	{
		String text = new String();
		String strLine = new String(" ");

		try
		{
			FileInputStream fstream = new FileInputStream(file);
			DataInputStream in = new DataInputStream(fstream);

			InputStreamReader isr = new InputStreamReader(in, "UTF-8");

			BufferedReader br = new BufferedReader(isr);

			while ((strLine =  br.readLine()) != null)
			{
				System.out.println (strLine);

				text += strLine;
			}
			br.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return text;
	}
}
