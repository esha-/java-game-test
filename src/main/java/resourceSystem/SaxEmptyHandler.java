package resourceSystem;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.*;
import java.lang.reflect.Member;


public class SaxEmptyHandler extends DefaultHandler
{

	private static String CLASSNAME = "class";
	private boolean inElement = false;
	private static String element = new String("");
	private Object object;

	public Object getInstance()
	{
		return object;
	}

	public void startDocument()
	{
		System.out.println("Start document parse ... ");
	}

	public void startElement(	String uri,
								String localName,
								String qName,
								Attributes attributes)	throws  SAXException
	{
		System.out.println("Start element \"" + qName + "\"");
		if(qName != CLASSNAME)
		{
			inElement = true;
			element = qName;
		}
		else
		{
			inElement = false;
			try
			{
				String className = attributes.getValue(0);
				System.out.println("Class Name \"" + className + "\"");
				object = ReflectionHelper.createInstance(className);
			}
			catch(InstantiationException e)
			{
				e.printStackTrace();
			}
			catch(IllegalAccessException e)
			{
				e.printStackTrace();
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}



		}
	}

	public void endElement(String uri, String localName, String qName) throws SAXException
	{
		//System.out.println("End element \"" + qName + "\"");
		//inElement = false;
		element = null;
	}

	public void characters(char []ch, int start, int length) throws SAXException
	{
		if(element != null && inElement)
		{
			try
			{
				String value = new String(ch,start,length);
				System.out.println("element: \"" + value + "\"");
				ReflectionHelper.setFieldValue(object, element, value);
			}
			catch(InstantiationException e)
			{
				e.printStackTrace();
			}
			catch(IllegalAccessException e)
			{
				e.printStackTrace();
			}
			catch(NoSuchFieldException e)
			{
				e.printStackTrace();
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}
		}
	}

}
