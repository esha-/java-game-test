package resourceSystem;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.io.*;

public class ResourceFactory
{
	private static ResourceFactory resourceFactory;

	public synchronized static ResourceFactory getInstance()
	{
		if(resourceFactory == null)
		{
			return (new ResourceFactory());
		}
		return resourceFactory;
	}
	public synchronized Object getResource(String path) throws Exception
	{
		Object object;
		SAXParserFactory factory = SAXParserFactory.newInstance();
    	SAXParser parser = factory.newSAXParser();
    	SaxEmptyHandler saxp = new SaxEmptyHandler();



    	//VFSImpl vfs = new VFSImpl("");

    	//parser.parse(vfs.getUFT8Text(path),saxp);
    	parser.parse(path,saxp);

    	object = saxp.getInstance();

		return object;
	}
}
