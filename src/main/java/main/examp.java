package main;

import org.eclipse.jetty.server.Server;

import resourceSystem.*;
import messageSystem.*;
import frontend.*;
import accountService.*;
import gameMechanics.*;

public class examp
{
    public static void main(String[] args) throws Exception
    {

    	String path = "resources/test.txt";

    	Resource resource = (Resource)ResourceFactory.getInstance().getResource(path);

    	AddressSystem addressSystem = new AddressSystem(resource.getAmountOfAS());

    	MsgSystem messageSystem = new MsgSystem(addressSystem);

    	FrontEnd frontend;

    	(new Thread(frontend = new FrontEnd(messageSystem,addressSystem,resource))).start();
    	Thread accountService = new Thread(new AccountService(messageSystem,addressSystem));
    	Thread gameMechanics = new Thread(new GameMechanics(messageSystem,addressSystem,resource));

    	accountService.start();
    	gameMechanics.start();

        Server server = new Server(resource.getServerPort());
        server.setHandler(frontend);
        server.start();
        server.join();

    }
}



