package timer;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import modules.Session;

public class RemindTask extends TimerTask
{
	private ConcurrentMap<Integer, ?> gameSessionToSession =
						new ConcurrentHashMap<Integer, Session>();
	private int clearTime;

	public RemindTask(ConcurrentMap<Integer, ?> gameSessionToSession,int clearTime)
	{
		this.clearTime = clearTime;
		this.gameSessionToSession = gameSessionToSession;
	}

	public void run()
	{
		Set st = gameSessionToSession.entrySet();

		long currentTime = new Date().getTime();

        Iterator it = st.iterator();
        while (it.hasNext())
        {
            Map.Entry<Integer,Session> m = (Map.Entry) it.next();
            Session usSesh =  m.getValue();
            long a =currentTime -  usSesh.getLastUseOfSession();
            if ( (currentTime -  usSesh.getLastUseOfSession())/1000 > clearTime)
            {
            	gameSessionToSession.remove(usSesh.getSessionId());
            }
        }
	}
}
