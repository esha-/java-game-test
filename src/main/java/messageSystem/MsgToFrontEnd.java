package messageSystem;

import modules.*;
import frontend.*;


public abstract class MsgToFrontEnd extends Msg
{
	public MsgToFrontEnd(Address from, Address to)
	{
		super(from,to);
	}

	public void exec(Abonent abonent)
	{
		if(abonent instanceof FrontEnd)
		{
			exec((FrontEnd)abonent);
		}
	}

	public abstract void exec(FrontEnd frontend);
}
