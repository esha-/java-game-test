package messageSystem;

import modules.*;
import resourceSystem.*;
import frontend.*;
import gameMechanics.*;
import main.*;
import timer.*;
import accountService.*;

import java.util.*;
import java.util.concurrent.*;



public class MsgSystem
{
	private static Map<Address,ArrayBlockingQueue<Msg>> messages
	= new HashMap<Address, ArrayBlockingQueue<Msg>>();


	public MsgSystem( AddressSystem addressSystem)
	{
		messages.put(addressSystem.getFEId(),new ArrayBlockingQueue<Msg>(200));
		messages.put(addressSystem.getASId(),new ArrayBlockingQueue<Msg>(200));
		messages.put(addressSystem.getGMId(),new ArrayBlockingQueue<Msg>(200));
	}

	public boolean isEmptyMsgQueue()
	{
		return messages.isEmpty();
	}

	public void sendMessage(Msg message)
	{
		Queue<Msg> messageQueue = messages.get(message.getTo());

		messageQueue.add(message);
	}
	public Msg getFirstMessageForAbonent(Abonent abonent)
	{
		Queue<Msg> messageQueue = messages.get(abonent.getAddress());
		Msg message = messageQueue.poll();
		return message;
	}
	public void execForAbonent(Abonent abonent)
	{
		Queue<Msg> messageQueue = messages.get(abonent.getAddress());
		while(!messageQueue.isEmpty())
		{
			Msg message = messageQueue.poll();
			message.exec(abonent);
		}
	}
}
