package messageSystem;

import java.util.*;

import modules.*;
import messageSystem.*;
import resourceSystem.*;
import frontend.*;
import gameMechanics.*;
import main.*;
import timer.*;

public class AddressSystem
{
	private int currentASId;
	//private int currentFEId;
	public static LinkedList<Address> asIds = new LinkedList<Address>();
	//private static LinkedList<Address> feIds = new LinkedList<Address>();
	private Address feIds;
	private Address GMs;

	public AddressSystem(int amountOfASs)
	{
		for(int i=0;i<amountOfASs;i++)
		{
			asIds.add(new Address());
		}

		//feIds.add(new Address());
		feIds = new Address();
		GMs = new Address();

		currentASId = 0;
		//currentFEId = 0;
	}

	public AddressSystem(int amountOfASs, int amountOfFEs)
	{
		this(amountOfASs);
		for(int i=0;i<amountOfFEs-1;i++)
		{
			;//feIds.add(new Address());
		}
	}

	public void setCurrentASId (int value) { 
		currentASId = value;		
	}
	
	public int getCurrentASId () { 
		return currentASId;		
	}
	
	
	public Address getASId()
	{
		if((currentASId+1) >= asIds.size())
		{
			currentASId = 0;
		}
		else
		{
			currentASId++;
		}
		return asIds.get(currentASId);
	}

	public Address getFEId()
	{
		return feIds;
	}

	public Address getGMId()
	{
		return GMs;
	}

	/*
	public Address getFEId()
	{
		if((currentASId+1) >= asIds.size())
		{
			currentFEId = 0;
		}
		else
		{
			currentFEId++;
		}
		return feIds.get(currentFEId);
	}
	public Address getFEIdByIndex(int index)
	{
		if(index > feIds.size()-1)
		{
			return null;
		}
		return feIds.get(index);
	}
	*/
	public Address getASIdByIndex(int index)
	{
		if(index > asIds.size()-1) 
		{
			return null;
		}
		return asIds.get(index);
	}
}
