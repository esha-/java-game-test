package modules;

public interface Session
{
	 long getLastUseOfSession();
	 int getSessionId();
}
