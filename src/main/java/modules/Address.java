package modules;

import java.util.concurrent.atomic.AtomicInteger;

public class Address
{
	static private AtomicInteger abonentIdCreator = new AtomicInteger();
	final private int abonentId;

	public Address()
	{
		abonentId = abonentIdCreator.incrementAndGet();
	}

	public int getAbonentId()
	{
		return abonentId;
	}
}
