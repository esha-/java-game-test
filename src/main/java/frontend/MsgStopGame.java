package frontend;

import modules.*;
import messageSystem.*;


public class MsgStopGame extends MsgToFrontEnd
{
	int firstUserSession;
	int secondUserSession;

	public MsgStopGame(Address from, Address to,
						int firstUserSession,
						int secondUserSession)
	{
		super(from,to);

		this.firstUserSession = firstUserSession;
		this.secondUserSession = secondUserSession;

	}

	public void exec(FrontEnd frontend)
	{
		frontend.stopGame(firstUserSession, secondUserSession);
	}

}
