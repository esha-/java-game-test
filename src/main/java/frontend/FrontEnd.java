package frontend;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Queue;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

//import com.sun.org.apache.xml.internal.serializer.utils.Messages;

import modules.*;
import messageSystem.*;
//import modules.PageGenerator;
import resourceSystem.*;
import gameMechanics.*;
import main.*;
import timer.*;
import accountService.*;

public class FrontEnd extends AbstractHandler implements Runnable, Abonent
{
	private MsgSystem messageSystem;
		
	private AddressSystem addressSystem;
	
	private Resource resource;
	
	static private AtomicInteger userSessionIdCreator = new AtomicInteger();
	
	enum RequestType 
	{
		U_NAME,
		U_ID,
		UNKNOWN,
		WAIT_ENEMY,
		RELOAD,
		CLICK,
		GAME_START;
	} 
	
	private PageGenerator pageGen = new PageGenerator();
	
	public static ConcurrentMap<Integer,UserSession> userSessionToSession = 
										new ConcurrentHashMap<Integer,UserSession>();
	
	public static Map<Integer,UserSession> readyForGameUsers = 
										new HashMap<Integer,UserSession>();
	
	
	public FrontEnd(MsgSystem messageSystem, AddressSystem addressSystem, Resource resource)
	{
		this.addressSystem = addressSystem;
		
		this.messageSystem = messageSystem;
		
		this.resource = resource;
	}
	
	public Address getAddress()
	{
		return addressSystem.getFEId();
	}
	
	private int getUserIdBySessionId(int sessionId)
	{
		int a = userSessionToSession.get(sessionId).getUserId();
		return (userSessionToSession.get(sessionId)).getUserId();
	}
	
	private int getEnemyBySessionId(int sessionId)
	{
		return userSessionToSession.get(sessionId).getEnemySessionId();
	}
	
	private int getGameSessionBySessionId(int sessionId)
	{
		return userSessionToSession.get(sessionId).getGameSessionId();
	}
	
	public void updateUserId(int sessionId, int userId)
	{
		(userSessionToSession.get(sessionId)).setUserId(userId);
		System.out.println("Hru");
	}
	
	
	public boolean IsUserWait(String userName)
	{
		Set st = userSessionToSession.entrySet();

        Iterator it = st.iterator();
        while (it.hasNext()) 
        {
            Map.Entry<Integer,UserSession> m = (Map.Entry) it.next();
            UserSession usSesh =  m.getValue();
            if (usSesh.getUserName().equals(userName) )
            {
            	if(usSesh.getUserId() == 0)
            	{
            		return true;
            	}
            }
        }
		
		return false;
	}
	
	private UserSession getUserSessionBySessionId(int sessionId)
	{
		return userSessionToSession.get(sessionId);
	}
	
	public boolean IsUserInMapUserSession(String userName)
	{
		Set st = userSessionToSession.entrySet();

        Iterator it = st.iterator();
        while (it.hasNext()) 
        {
            Map.Entry<Integer,UserSession> m = (Map.Entry) it.next();
            UserSession usSesh =  m.getValue();
            if (usSesh.getUserName().equals(userName) )
            {
            	return true;
            }
        }
		
		return false;
	}
	
	public UserSession findAnotherReadyUser( int sessionId)
	{
		Set st = readyForGameUsers.entrySet();

        Iterator it = st.iterator();
        while (it.hasNext()) 
        {
            Map.Entry<Integer,UserSession> m = (Map.Entry) it.next();
            UserSession usSesh =  m.getValue();
            if (usSesh.getSessionId() != sessionId && usSesh.getEnemySessionId() == 0)
            {
            	return usSesh;
            }
        }
		
		return null;
	}
	
	public RequestType checkRequest(Request baseRequest,UserSession userSession)
	{
		
		RequestType reqType ;
		Map<String,String[]> mp = baseRequest.getParameterMap();
		String inputName;
		int sessionID;
		UserSession usS;
		String s = baseRequest.getMethod();
		/*if(mp.containsKey(new String("only_id")))
		{
			reqType = RequestType.U_ID;
			inputName = (mp.get("only_id"))[0];
			int num = Integer.parseInt(inputName);
			userSession.setUserId(num);
			
			inputName = (mp.get("session_id"))[0];
			num = Integer.parseInt(inputName);
			userSession.setSessionId(num);
			return reqType;
		}*/
		
		if(mp.containsKey(new String("u_name")))
		{ 
		
			reqType = RequestType.U_NAME;
			inputName = (mp.get("u_name"))[0];
			userSession.setUserName(inputName);
			sessionID = Integer.parseInt(mp.get("uid_with_name")[0]);
			
			userSession.setSessionId(sessionID);
			
			return reqType;
		}
		if(mp.containsKey(new String("wait_enemy")))
		{
			sessionID = Integer.parseInt(mp.get("uid_with_name")[0]);
			userSession.setSessionId(sessionID);
			
			
			if(!userSessionToSession.containsKey(sessionID))
			{
				return reqType = RequestType.UNKNOWN;
			}
			
			userSessionToSession.get(sessionID).updateLastUse();
			
			if ( readyForGameUsers.isEmpty())
			{
				readyForGameUsers.put(sessionID, new UserSession("",sessionID));
				reqType = RequestType.WAIT_ENEMY;
			}
			else if(readyForGameUsers.containsKey(sessionID))
			{
				if(readyForGameUsers.get(sessionID).getEnemySessionId() != 0)
				{
					reqType = RequestType.GAME_START;
				}
				else
				{
					reqType = RequestType.WAIT_ENEMY;
				}
			}
			else
			{
				if( (usS = findAnotherReadyUser(sessionID)) == null )
				{
					reqType = RequestType.WAIT_ENEMY;
				}
				else
				{
					usS.setEnemySessionId(sessionID);
					userSession.setEnemySessionId(usS.getSessionId());
					getUserSessionBySessionId(sessionID).setEnemySessionId(usS.getSessionId());
					getUserSessionBySessionId(usS.getSessionId()).setEnemySessionId(sessionID);
					reqType = RequestType.GAME_START;
				}
			}
			return reqType;
		}
		if(mp.containsKey(new String("game_reload")))
		{
			reqType = RequestType.RELOAD;
			sessionID = Integer.parseInt(mp.get("g_s_session_id")[0]);
			userSession.setSessionId(sessionID);
			return reqType;
		}
		
		if(mp.containsKey(new String("game_click")))
		{
			reqType = RequestType.CLICK;
			sessionID = Integer.parseInt(mp.get("g_s_session_id")[0]);
			userSession.setSessionId(sessionID);
			return reqType;
		}
		else 
		{
			reqType = RequestType.UNKNOWN;
			return reqType;
		}
	}
	
	void stopGame(int firstUserSession, int secondUserSession)
	{
		userSessionToSession.get(firstUserSession).setStopGame(true);
	}
	
	long getGameTimeByUserSessionId(int sessionId)
	{
		return userSessionToSession.get(sessionId).getGameTime();
	}
	
	void gameCanStarted(int sessionIdFirst, int firstUserId,
						int sessionIdSecond, int secondUserId, int gameSession )
	{
		userSessionToSession.get(sessionIdFirst).setGameSessionId(gameSession);
		userSessionToSession.get(sessionIdSecond).setGameSessionId(gameSession);
	}
	
	void updateGameState(int firstUserSessionId, int firstUserId,
						int secondUserSessionId, int secondUserId,
						int numFirstUserClicks, int numSecondUserClicks,
						int gameSessionId, long gameTime)
	{
		userSessionToSession.get(firstUserSessionId).setClicks(numFirstUserClicks);
		userSessionToSession.get(secondUserSessionId).setClicks(numSecondUserClicks);
		userSessionToSession.get(firstUserSessionId).setGameSessionId(gameSessionId);
		userSessionToSession.get(secondUserSessionId).setGameSessionId(gameSessionId);
		
		userSessionToSession.get(firstUserSessionId).setGameTime(gameTime);
		userSessionToSession.get(secondUserSessionId).setGameTime(gameTime);
		
		//pageGen.setSessionId(firstUserSessionId);
		
		
	}
	
	void clearUserGameDate(int sessionId)
	{
		userSessionToSession.get(sessionId).clearUserGameDate();
	}
	
	int getClicksBySessionId(int sessionId)
	{
		return userSessionToSession.get(sessionId).getClicks();
	}
	
	public boolean isVin(int sessionId)
	{
		return userSessionToSession.get(sessionId).getClicks() > userSessionToSession.get(getEnemyBySessionId(sessionId)).getClicks();
	}
	
    public void handle(String target,Request baseRequest,HttpServletRequest request,HttpServletResponse response) 
        throws IOException, ServletException
    {    	
    	PageGenerator pageGen = new PageGenerator();
    	UserSession userSession = new UserSession();
    	
    	RequestType reqType;
    	String userName = new String("");
    	int userId;
        	
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        
        reqType = checkRequest(baseRequest,userSession);
        baseRequest.setHandled(true);
        
        switch(reqType)
        {
        case U_NAME:
			
        	if( IsUserInMapUserSession(userSession.getUserName()) && IsUserWait(userSession.getUserName()) )
        	{
        		response.getWriter().println(pageGen.getWaitPage(userSession.getUserName(),userSession.getSessionId()));
        		break;
        	}
        	
        	if( ! IsUserInMapUserSession(userSession.getUserName()) )
        	{
        		messageSystem.sendMessage( new MsgChechUserName(addressSystem.getFEId(),
																addressSystem.getASId(),
																userSession.getSessionId(),
																userSession.getUserName()) );
				userSessionToSession.put(userSession.getSessionId(), userSession);
				response.getWriter().println(pageGen.getWaitPage(userSession.getUserName(),userSession.getSessionId()));
				break;
        	}
        	
        //case U_ID:
        	
        	userId = userSessionToSession.get(userSession.getSessionId()).getUserId();
        
        	userName = userSessionToSession.get(userSession.getSessionId()).getUserName();
        	response.getWriter().println( //"Hello , " + userName + " !  Your ID:  " + userId +
        									 pageGen.getStartButtonPage(userName,userId,userSession.getSessionId()) );
        	

        	break;
        case UNKNOWN:
        
        	response.getWriter().println(pageGen.getNamePage(userSessionIdCreator.incrementAndGet()));
        	break;
        case WAIT_ENEMY:
        
        	response.getWriter().println(pageGen.getWaitEnamyPage(userSession.getSessionId()));
        	break;
        case GAME_START:
        	if(readyForGameUsers.containsKey(userSession.getSessionId()))
        	{
        		readyForGameUsers.remove(userSession.getSessionId());
        	}
        	else
        	{
        	messageSystem.sendMessage(new MsgStartGameSession(addressSystem.getFEId(),
        														addressSystem.getGMId(),
        														userSession.getSessionId(),
        														getUserIdBySessionId(userSession.getSessionId()),
        														userSession.getEnemySessionId(),
        														getUserIdBySessionId(userSession.getEnemySessionId())
        														));
        	}
        	
        	
        	response.getWriter().println( pageGen.getGamePage(0,0,0,userSession.getSessionId()) );
        	break;
        	
        	case RELOAD:
        		int mySessionId = userSession.getSessionId();
        		int enemySessionId = getEnemyBySessionId(mySessionId);
        		int myScore = userSessionToSession.get(mySessionId).getClicks();
        		int enemyScore = userSessionToSession.get(enemySessionId).getClicks();
        		/*
        		while(getGameSessionBySessionId(userSession.getSessionId()) == 0)
        		{
		       		try
		          	{
		       			Thread.sleep(50);
		      		}
		       		catch(InterruptedException e)
	         		{
	          			e.printStackTrace();
	          		}
   				}*/
        				if(  userSessionToSession.get(mySessionId).getStopGame()  )
        				{
        					messageSystem.sendMessage(new MsgAddStatisticAboutUser(addressSystem.getFEId(),
        																			addressSystem.getASId(),
        																			getUserIdBySessionId(mySessionId),
        																			getClicksBySessionId(mySessionId),
        																			isVin(mySessionId)
        																			));
        					
        					response.getWriter().println( pageGen.getStopGamePage(myScore, enemyScore,
        																			userSessionToSession.get(mySessionId).getClicks(),
        																			userSessionToSession.get(enemySessionId).getClicks(),
        																			mySessionId
        																			));
        					clearUserGameDate(mySessionId);
        				}
        				else
        				{
			        		messageSystem.sendMessage(new MsgGetGameState(addressSystem.getFEId(),
			        														addressSystem.getGMId(),
			        														userSession.getSessionId(),
			        														getUserIdBySessionId(userSession.getSessionId()),
			        														enemySessionId=getEnemyBySessionId(userSession.getSessionId()),
			        														getUserIdBySessionId(enemySessionId),
			        														getGameSessionBySessionId(userSession.getSessionId())
			        														));
			        														
			        		
			        		response.getWriter().println( pageGen.getGamePage(getUserSessionBySessionId(userSession.getSessionId()).getClicks(),
			        										getUserSessionBySessionId(getEnemyBySessionId(userSession.getSessionId())).getClicks(),
			        										getGameTimeByUserSessionId(userSession.getSessionId()),
			        										userSession.getSessionId()));
        				}
        	break;
        	
        	case CLICK:
        		int mySession = userSession.getSessionId();
        		int enemySession = getEnemyBySessionId(mySession);
        		int mScore = userSessionToSession.get(mySession).getClicks();
        		int enemScore = userSessionToSession.get(enemySession).getClicks();
        		
        		
        		if(  userSessionToSession.get(mySession).getStopGame()  )
        		{
        			messageSystem.sendMessage(new MsgAddStatisticAboutUser(addressSystem.getFEId(),
																			addressSystem.getASId(),
																			getUserIdBySessionId(mySession),
																			getClicksBySessionId(mySession),
																			isVin(mySession)
																			));
        			response.getWriter().println( pageGen.getStopGamePage(mScore, enemScore,
							userSessionToSession.get(mySession).getClicks(),
							userSessionToSession.get(enemySession).getClicks(),
							mySession
							));
        			clearUserGameDate(mySession);
        		}
        		else
        		{
	        		messageSystem.sendMessage(new MsgAddClick(addressSystem.getFEId(),
	        													addressSystem.getGMId(),
	        													userSession.getSessionId(),
	        													getGameSessionBySessionId(userSession.getSessionId()),
	        													getEnemyBySessionId(userSession.getSessionId())));
	        		
	        		
	        		response.getWriter().println( pageGen.getGamePage(getUserSessionBySessionId(userSession.getSessionId()).getClicks(),
													(getUserSessionBySessionId(getEnemyBySessionId(userSession.getSessionId())).getClicks()),
														getGameTimeByUserSessionId(userSession.getSessionId()),
														userSession.getSessionId()
														));
        		}
        		break;
        }
        
    }
    
    boolean checkReadyUsers()
    {
    	return true;//readyForGameUsers
    }
    
    
    public void run()
    {
   	
    	new ClearGameSessionList(userSessionToSession,resource.getTimeForClear());
    	
    	while(true)
    	{
    		messageSystem.execForAbonent(this);
    		try
    		{
    			Thread.sleep(10);
    		}
    		catch(InterruptedException e)
    		{
    			e.printStackTrace();
    		}
    	}
    }
}
