package frontend;

import java.util.Date;

import modules.*;


public class UserSession implements Session
{
	private String userName;
	private int sessionId;
	private int userId;
	private int enemySessionId;
	private int clicks;
	private int gameSessionId;
	private long gameTime;

	long lastUseOfSession;

	private boolean stopGame;

	public UserSession(String userName, int sessionId)
	{
		lastUseOfSession = (new Date()).getTime();
		this.userName = userName;
		this.sessionId = sessionId;
		//this.enemySessionId = enemySessionId;
	}
	public UserSession()
	{
		this.userName = new String("");
		this.sessionId = 0;
		this.userId = 0;
		//lastUseOfSession = (new Date()).getTime();
		lastUseOfSession =0;
	}
	 String getUserName()
	 {
		 lastUseOfSession = (new Date()).getTime();
		 return userName;
	 }

	 public void setStopGame(boolean stopGame)
	 {
		 this.stopGame = stopGame;
	 }

	 public boolean getStopGame()
	 {
		 lastUseOfSession = (new Date()).getTime();
		 return stopGame;
	 }

	 public void setGameTime(long gameTime)
	 {
		 lastUseOfSession = (new Date()).getTime();
		 this.gameTime = gameTime;
	 }

	 public long getGameTime()
	 {
		 lastUseOfSession = (new Date()).getTime();
		 return gameTime;
	 }


	 public int getGameSessionId()
	 {
		 lastUseOfSession = (new Date()).getTime();
		 return gameSessionId;
	 }

	 public void setGameSessionId(int gameSessionId)
	 {
		 lastUseOfSession = (new Date()).getTime();
		 this.gameSessionId = gameSessionId;
	 }

	 public void setClicks(int clicks)
	 {
		 lastUseOfSession = (new Date()).getTime();
		 this.clicks = clicks;
	 }

	 public void setEnemySessionId(int enemySessionId)
	 {
		 lastUseOfSession = (new Date()).getTime();
		 this.enemySessionId = enemySessionId;
	 }

	 public int getEnemySessionId()
	 {
		 lastUseOfSession = (new Date()).getTime();
		 return enemySessionId;
	 }

	 public int getSessionId()
	 {
		 lastUseOfSession = (new Date()).getTime();
		 return sessionId;
	 }

	 public int getUserId()
	 {
		 lastUseOfSession = (new Date()).getTime();
		 return userId;
	 }

	 public  void setUserId( int userId)
	 {
		 lastUseOfSession = (new Date()).getTime();
		 this.userId = userId;
	 }

	 public void setSessionId( int sessionId)
	 {
		 lastUseOfSession = (new Date()).getTime();
		 this.sessionId = sessionId;
	 }
	 public void setUserName( String userName)
	 {
		 lastUseOfSession = (new Date()).getTime();
		 this.userName = userName;
	 }
	 public int getClicks()
	 {
		 lastUseOfSession = (new Date()).getTime();
		 return clicks;
	 }

	 public void clearUserGameDate()
	 {
		 stopGame = false;
		 gameTime = 0;
		 gameSessionId = 0;
		 //clicks = 0;
		 enemySessionId = 0;
		 lastUseOfSession =0;
		 userName="";
	 }

	public  long getLastUseOfSession()
		{
			return lastUseOfSession;
		}
	 void updateLastUse()
	 {
		 lastUseOfSession = (new Date()).getTime();
	 }

}
