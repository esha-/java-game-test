package frontend;



public class PageGenerator
{
	/*static int help = 0;
	int userId;
	int sessionId;
	int userClicks;
	int enemyClicks;

	String u_name;
	String page;
	*/
	String page;
	PageGenerator()
	{

	}
	String getNamePage(int sessionId)
	{
		return page = new String("<html>" +
									"<body>" +
										"<p><b>Please, insert your name:</b><br>" +
										"<form name=\"user_name\" method=\"POST\">" +
										"<input type=\"hidden\" name=\"uid_with_name\" value=\"" +
										sessionId +
										"\">" +
										"<input type=\"text\" name=\"u_name\" size=25>" +
										"</form>" +
									"</body>" +
								"</html>");
	}
	String getWaitPage(String u_name, int sessionId)
	{
		return page = new String("<html>" +
									"<body>" +
										"<p><b>Wait,please.Yout request is processed</b><br>" +
										"<form name=\"u_wait\"  method=\"POST\">" +
										"<input type=\"hidden\" name=\"u_name\" value=\"" +
										u_name +
										"\">" +
										"<input type=\"hidden\" name=\"uid_with_name\" value=\"" +
										sessionId +
										"\">" +
										"</form>" +
										"<script language=\"JavaScript\" type=\"text/javascript\">" +
				        					"function formBack()" +
				        					"{" +
				        						"u_wait.submit()" +
				        					"}" +
				        					"setTimeout('formBack()', 500)"+
			        					"</script>" +
									"</body>" +
								"</html>");
	}

	String getWaitEnamyPage(int sessionId)
	{
		return page = new String("<html>" +
									"<body>" +
										"<p><b>Wait,please.Yout request is processed</b><br>" +
										"<form name=\"u_wait\"  method=\"POST\">" +
										"<input type=\"hidden\" name=\"wait_enemy\" " +
										">" +
										"<input type=\"hidden\" name=\"uid_with_name\" value=\"" +
										sessionId +
										"\">" +
										"</form>" +
										"<script language=\"JavaScript\" type=\"text/javascript\">" +
				        					"function formBack()" +
				        					"{" +
				        						"u_wait.submit()" +
				        					"}" +
				        					"setTimeout('formBack()', 500)"+
			        					"</script>" +
									"</body>" +
								"</html>");
	}


	String getStartButtonPage(String userName, int userId,int sessionId)
	{
		return page = new String("<html>" +
									"<body>" +
										//"<p><b></b><br>" +
										"Hello , " + userName + " !  Your ID:  " + userId +
										"<form name=\"ready\"  method=\"POST\">" +
										"<input type=\"hidden\" name=\"wait_enemy\" >" +
										"<input type=\"hidden\" name=\"uid_with_name\" value=\"" +
										sessionId +
										"\">" +
										"<input type=\"submit\" value=\"Start Game\">" +
										"</form>" +
									"</body>" +
								"</html>");
	}

	String getGamePage(int myClicks, int enemyClicks, long time,int sessionId)
	{
		String myColor = myClicks>enemyClicks?(new String("<font color=\"red\" size = 20>")):(new String("<font color=\"blue\" size = 20>"));
		String enemyColor = myClicks>enemyClicks?(new String("<font color=\"blue\" size = 20>")):(new String("<font color=\"red\" size = 20>"));
		return page = new String("<html>" +
									"<body>" +
										//"<p><b></b><br>" +
										"<form name=\"reload\"  method=\"POST\">" +
											"<input type=\"hidden\" name=\"game_reload\" >" +
											"<input type=\"hidden\" name=\"g_s_session_id\" value=\"" +
											sessionId +
											"\">" +
										"</form>" +
										"<form name=\"click\"  method=\"POST\">" +
											"<input type=\"hidden\" name=\"game_click\" >" +
											"<input type=\"hidden\" name=\"g_s_session_id\" value=\"" +
											sessionId +
											"\">" +
											"<br><br><br>" +
											"<table width=\"80%\" border=\"2\" align=\"center\">" +
												"<tr align=\"center\">"+
													"<td>" +
														"<p><b>" + myColor + myClicks + "</font></b><br>" +
														"<input type=\"submit\" value=\"\nClick HERE\n\">" +
													"</td>"+
													"<td>" +
														"<p><b><h1>Time remain:<br>"+time+"</h1></b><br>" +
													"</td>"+
													" <td>" +
														"<p><b>"+enemyColor+enemyClicks+"</font></b><br>" +
														"<p><b><h1>Enemy's Clicks</h1></b><br>" +
													"</td>" +
												"</tr>" +
											"</table>"+
										"</form>" +
										"<script language=\"JavaScript\" type=\"text/javascript\">" +
											"function formBack()" +
												"{" +
													"reload.submit()" +
												"}" +
											"setTimeout('formBack()', 500)"+
										"</script>" +
									"</body>" +
								"</html>");
	}

	String getStopGamePage(int myScore, int enemyScore, int myClicks, int clicksEnemy,int sessionId)
	{

		String myColor = myClicks>clicksEnemy?(new String("red")):(new String("blue"));
		//String enemyColor = myClicks>enemyClicks?(new String("<font color=\"blue\" size = 20>")):(new String("<font color=\"red\" size = 20>"));
		String status = myScore>enemyScore?(new String("You WIN!")):(new String("You are LOOOSER"));
		return page = new String("<html>" +
									"<body>" +
										"<form name=\"stop_game\"  method=\"POST\">" +
										"<input type=\"hidden\" name=\"wait_enemy\" >" +
										"<input type=\"hidden\" name=\"uid_with_name\" value=\"" +
										sessionId +
										"\">" +
										"<p><b>" +
										"<br><br><br><font align=\"right\" color=\"" + myColor + "\" size = 50>" + status  +
										"</b><br>"+
										"<input type=\"submit\" value=\"Begin One More Time ?\">" +
										"</form>" +
										"<table width=\"80%\" border=\"2\" align=\"center\">" +
										"<tr align=\"center\">"+
											"<td>" +
												"<p><b><h1>"+myClicks +
												"<br><br>Your Clicks" + "</h1></b><br><b>" +
											"</td>"+
											"<td>" +
												"<p><b><h1>Time remain:<br>"+0+"</h1></b><br>" +
											"</td>"+
											" <td>" +
												"<p><b><h1 color = \"red\">"+ clicksEnemy +"</h1></b>" +
												"<p><b><h1>Enemy's Clicks</h1></b><br>" +
											"</td>" +
										"</tr>" +
									"</table>"+
									"</body>" +
								"</html>");
	}
}
