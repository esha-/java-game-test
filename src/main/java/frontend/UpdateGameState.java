package frontend;

import modules.*;
import messageSystem.*;


public class UpdateGameState extends MsgToFrontEnd
{
	final private int firstUserSessionId;
	final private int firstUserId;

	final private int secondUserSessionId;
	final private int secondUserId;

	private int numFirstUserClicks;
	private int numSecondUserClicks;

	private int gameSessionId;

	private long gameTime;

	public UpdateGameState(Address from, Address to,
							int firstUserSessionId, int firstUserId,
							int secondUserSessionId, int secondUserId,
							int numFirstUserClicks, int numSecondUserClicks,
							int gameSessionId, long gameTime)
	{
		super(from,to);

		this.firstUserSessionId = firstUserSessionId;
		this.firstUserId = firstUserId;

		this.secondUserSessionId = secondUserSessionId;
		this.secondUserId = secondUserId;

		this.numFirstUserClicks = numFirstUserClicks;
		this.numSecondUserClicks = numSecondUserClicks;

		this.gameSessionId = gameSessionId;

		this.gameTime = gameTime;
	}

	public void exec(Abonent abonent)
	{
		if(abonent instanceof FrontEnd)
		{
			exec((FrontEnd)abonent);
		}
	}

	public  void exec(FrontEnd frontend)
	{
		frontend.updateGameState(firstUserSessionId, firstUserId,
								secondUserSessionId, secondUserId,
								numFirstUserClicks, numSecondUserClicks,
								gameSessionId, gameTime
								);
	}
}
