package frontend;

import modules.*;
import messageSystem.*;


public class MsgUpdateUserId  extends MsgToFrontEnd
{
	final private int sessionId;
	final private int userId;

	public MsgUpdateUserId(Address from, Address to,int sessionId, int userId)
	{
		super(from,to);
		this.sessionId = sessionId;
		this.userId = userId;
	}

	public void exec(FrontEnd frontEnd)
	 {
		frontEnd.updateUserId(sessionId, userId);
	 }

}
