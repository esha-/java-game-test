package frontend;

import modules.*;
import messageSystem.*;


public class MsgGameCanStarted extends MsgToFrontEnd
{
	final private int sessionIdFirst;
	final private int firstUserId;

	final private int sessionIdSecond;
	final private int secondUserId;

	int gameSession;

	public MsgGameCanStarted(Address from, Address to,
							int sessionIdFirst, int firstUserId,
							int sessionIdSecond, int secondUserId, int gameSession )
	{
		super(from,to);
		this.sessionIdFirst = sessionIdFirst;
		this.firstUserId = firstUserId;

		this.sessionIdSecond = sessionIdSecond;
		this.secondUserId = secondUserId;

		this.gameSession = gameSession;
	}

	public void exec(FrontEnd frontEnd)
	 {
		frontEnd.gameCanStarted(sessionIdFirst, firstUserId,
								sessionIdSecond, secondUserId,gameSession);
	 }
}
