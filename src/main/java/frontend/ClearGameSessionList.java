package frontend;

import java.util.Timer;

import java.util.concurrent.ConcurrentMap;


import timer.*;

public class ClearGameSessionList
{
	private Timer timer;
	//private ConcurrentMap<Integer, GameSession> gameSessionToSession =
			//				new ConcurrentHashMap<Integer, GameSession>();
	//private int clearTime;

	ClearGameSessionList(ConcurrentMap<Integer, ?> gameSessionToSession,int clearTime)
	{
		//this.clearTime =clearTime;
		//this.gameSessionToSession = gameSessionToSession;
		timer = new Timer();
	    timer.schedule(new RemindTask(gameSessionToSession,clearTime), 10000, //initial delay
	    		clearTime*1000);
	}
}
