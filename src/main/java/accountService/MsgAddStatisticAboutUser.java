package accountService;

import modules.*;
import resourceSystem.*;
import frontend.*;
import gameMechanics.*;
import main.*;
import timer.*;
import messageSystem.*;

public class MsgAddStatisticAboutUser extends MsgToAccountService
{
	final private int userId;
	final private int  clicks ;
	final private boolean vin;

	public MsgAddStatisticAboutUser(Address from, Address to,int userId, int clicks, boolean vin)
	{
		super(from,to);
		this.userId = userId;
		this.clicks = clicks;
		this.vin = vin;
	}

	public void exec(AccountService accountService)
	 {
		accountService.addStatisticAboutUser(userId,clicks,vin);
	 }
}
