package accountService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicInteger;

import com.mysql.jdbc.Driver;

import modules.*;
import messageSystem.*;
import frontend.*;


public class AccountService implements Runnable,Abonent
{
	private MsgSystem messageSystem;

	private AddressSystem addressSystem;

	static private AtomicInteger userSessionIdCreator = new AtomicInteger();

	public AccountService(MsgSystem messageSystem, AddressSystem addressSystem)
	{
		this.addressSystem = addressSystem;

		this.messageSystem = messageSystem;
	}

	public Address getAddress()
	{
		return addressSystem.getASId();
	}

	public Connection getConnection() {
		try{
			DriverManager.registerDriver((Driver) Class.forName("com.mysql.jdbc.Driver").newInstance());

			StringBuilder url = new StringBuilder();

			url.
			append("jdbc:mysql://").		//db type
			append("localhost:"). 			//host name
			append("3306/").				//port
			append("USERS?").			//db name
			append("user=root&").			//login
			append("password= ");		//password

			Connection connection = DriverManager.getConnection(url.toString());
			return connection;
		} catch (Exception e) {
		return null;
	}
	}


	public void checkUserName(int sessionId,String userName)
	{
		Connection connection = null;
		ResultSet rs = null;
		String str = null;
		int userId=-2 ;
		try
		{
				connection = getConnection();

		    	Statement statement = connection.createStatement();

		    	String query = new String("select id from users where name=\""+ userName + "\";");

		    	rs = statement.executeQuery(query);

		    	if(!rs.next())
		    	{
		    		statement.executeUpdate("insert into users(name,data) values(\""+userName+"\",now());");

		    		rs = statement.executeQuery(query);
		    		rs.next();
		    	}

		    	str = rs.getString(1);

		    	userId = Integer.parseInt(str);

		    	rs.close();
		    	rs=null;
		    	connection.close(); connection=null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		messageSystem.sendMessage(new MsgUpdateUserId(addressSystem.getASId(),
														addressSystem.getFEId(),
														sessionId,
														userId));
	}

	public void addStatisticAboutUser(int userId, int clicks,boolean vin)
	{
		Connection connection = null;
		String str = null;
		Statement statement = null;
		String query = null;
		try
		{
				connection = getConnection();

				statement = connection.createStatement();

		    	query = new String("insert into users_statistic(user_id,clicks,data,vin) values(" +
		    			userId + "," + clicks + ",now()," + vin + ");");

		    	statement.executeUpdate(query);

		    	connection.close();
		    	connection=null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void run()
	{
		try
		{
			Thread.sleep(1000);
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}

		while (true)
		{
			messageSystem.execForAbonent(this);
			try
			{
				Thread.sleep(1000);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}

	}
}
