package accountService;


import modules.*;
import messageSystem.*;
import resourceSystem.*;
import frontend.*;
import gameMechanics.*;
import main.*;
import timer.*;

public class MsgChechUserName extends MsgToAccountService
{
	final private int sessionId;
	final private String userName ;

	public MsgChechUserName(Address from, Address to,int sessionId, String userName)
	{
		super(from,to);
		this.sessionId = sessionId;
		this.userName = userName;
	}

	public void exec(AccountService accountService)
	 {
		accountService.checkUserName(sessionId,userName);

	 }
}
