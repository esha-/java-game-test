package gameMechanics;

import modules.*;
import messageSystem.*;


public class MsgAddClick extends MsgToGameMechanics
{

	int sessionId;
	int gameSessionId;
	int sessionIdAdd;

	public MsgAddClick(Address from, Address to,
						int sessionIdAdd, int gameSessionId,int sessionId)
	{
		super(from,to);
		this.sessionId = sessionId;
		this.gameSessionId = gameSessionId;
		this.sessionIdAdd = sessionIdAdd;
	}

	public  void exec(GameMechanics gameMechanics)
	{
		gameMechanics.addClick(sessionIdAdd, gameSessionId, sessionId);
	}
	public int getSessionId()
	{
		return sessionId;
	}
	public int getGameSessionId()
	{
		return gameSessionId;
	}
	public int getSessionIdAdd()
	{
		return sessionIdAdd;
	}

}
