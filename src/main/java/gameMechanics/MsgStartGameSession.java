package gameMechanics;

import modules.*;
import messageSystem.*;


public class MsgStartGameSession extends MsgToGameMechanics
{
	final private int firstUserSessionId;
	final private int firstUserId;

	final private int secondUserSessionId;
	final private int secondUserId;

	public MsgStartGameSession(Address from, Address to,
								int firstUserSessionId, int firstUserId,
								int secondUserSessionId, int secondUserId)
	{
		super(from,to);

		this.firstUserSessionId = firstUserSessionId;
		this.firstUserId = firstUserId;

		this.secondUserSessionId = secondUserSessionId;
		this.secondUserId = secondUserId;
	}

	public void exec(GameMechanics gameMechanics)
	 {
		gameMechanics.startGameSession(firstUserSessionId, firstUserId,
										secondUserSessionId, secondUserId);
	 }
}
