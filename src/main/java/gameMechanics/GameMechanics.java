package gameMechanics;

//import java.io.IOException;
//import java.util.Iterator;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
//import java.util.Set;
import java.util.HashMap;
//import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import modules.*;
import messageSystem.*;
import resourceSystem.*;
import frontend.*;
import gameMechanics.*;
import main.*;
import timer.*;
import accountService.*;


public class GameMechanics implements Runnable,Abonent
{

	final public ConcurrentMap<Integer, GameSession> gameSessionToSession =
		new ConcurrentHashMap<Integer, GameSession>();

	static public AtomicInteger gameSessionIdCreator = new AtomicInteger();

	private MsgSystem messageSystem;

	private AddressSystem addressSystem;

	private Resource resource;

	public GameMechanics(MsgSystem messageSystem, AddressSystem addressSystem, Resource resource)
	{
		this.messageSystem = messageSystem;
		this.addressSystem = addressSystem;
		this.resource = resource;
	}

	public Address getAddress()
	{
		return addressSystem.getGMId();
	}
	
	public boolean isEmptyMessageQueue()
	{
		return messageSystem.isEmptyMsgQueue();
	}
	
	public boolean isEmptyGameSessionToSession()
	{
		return gameSessionToSession.isEmpty();
	}
	
	public void startGameSession(int firstUserSessionId, int firstUserId,
							int secondUserSessionId, int secondUserId )
	{
		
		int gameSessionId = gameSessionIdCreator.incrementAndGet();
		gameSessionToSession.put( gameSessionId,
									new GameSession(firstUserId,secondUserId,
													firstUserSessionId,secondUserSessionId,
													gameSessionId,
													resource.getTIME_FOR_GAME()) );
		messageSystem.sendMessage(new MsgGameCanStarted(addressSystem.getGMId(),
														addressSystem.getFEId(),
														firstUserSessionId,firstUserId,
														secondUserSessionId, secondUserId,
														gameSessionId));

	}

	void addClick(int sessionIdAdd, int gameSessionId,int sessionId)
	{
		getGameSessionByGSId(gameSessionId).addClickInMap(sessionIdAdd);

		GameSession gameSession = gameSessionToSession.get(gameSessionId);
		long time = gameSession.getTIME_FOR_GAME() - ((new Date()).getTime()-gameSession.getGameBeginTime())/1000;
		
		if(time < 0)
		{
			messageSystem.sendMessage(new MsgStopGame(addressSystem.getGMId(),
					addressSystem.getFEId(),
					sessionIdAdd,
					sessionId));
			//gameSessionToSession.remove(gameSessionId);
		}
		else
		{
			messageSystem.sendMessage(new UpdateGameState(addressSystem.getGMId(),
															addressSystem.getFEId(),
															sessionIdAdd, 0,
															sessionId, 0,
															gameSession.getClickBySessionId(sessionIdAdd),
															gameSession.getClickBySessionId(sessionId),
															gameSession.getGameSessionId(),
															time));
		}
	}

	void GetGameState(int firstUserSessionId, int firstUserId,
						int secondUserSessionId, int secondUserId, int gameSessionId)
	{
		GameSession gameSession = gameSessionToSession.get(gameSessionId);

		long time = gameSession.getTIME_FOR_GAME() - ((new Date()).getTime()-gameSession.getGameBeginTime())/1000;

		if(time < 0)
		{
			messageSystem.sendMessage(new MsgStopGame(addressSystem.getGMId(),
															addressSystem.getFEId(),
															firstUserSessionId,
															secondUserSessionId));
			//gameSessionToSession.remove(gameSessionId);
		}
		else
		{
			messageSystem.sendMessage(new UpdateGameState(addressSystem.getGMId(),
															addressSystem.getFEId(),
															firstUserSessionId, firstUserId,
															secondUserSessionId, secondUserId,
															gameSession.getClickBySessionId(firstUserSessionId),
															gameSession.getClickBySessionId(secondUserSessionId),
															gameSession.getGameSessionId(),
															time));
		}
	}

	public GameSession getGameSessionByGSId(int gameSessionId)
	{
		return gameSessionToSession.get(gameSessionId);
	}

	public int findGameSessionByUSession(int firstUserSession)
	{
		Set st = gameSessionToSession.entrySet();

        Iterator it = st.iterator();
        while (it.hasNext())
        {
            Map.Entry<Integer,GameSession> m = (Map.Entry) it.next();
            GameSession usSesh =  m.getValue();
            if (usSesh.getIdSessionOnFrontEndFirst() ==  firstUserSession)
            {
            		return usSesh.getGameSessionId();
            }
        }
        return 0;
	}


	public void run()
	{
		new ClearGameSessionList(gameSessionToSession,resource.getTimeForClear());
		while(true)
    	{
			long beginTime = new Date().getTime();

			messageSystem.execForAbonent(this);
			try
			{
				Thread.sleep(10);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}


    	}
	}
}
