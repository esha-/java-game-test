package gameMechanics;

import modules.*;
import messageSystem.*;


public class MsgGetGameState extends MsgToGameMechanics
{
	final private int firstUserSessionId;
	final private int firstUserId;

	final private int secondUserSessionId;
	final private int secondUserId;

	private int numFirstUserClicks;
	private int numSecondUserClicks;

	int gameSession;

	public MsgGetGameState(Address from, Address to,
			int firstUserSessionId, int firstUserId,
			int secondUserSessionId, int secondUserId,int gameSession)
	{
		super(from,to);

		this.firstUserSessionId = firstUserSessionId;
		this.firstUserId = firstUserId;

		this.secondUserSessionId = secondUserSessionId;
		this.secondUserId = secondUserId;

		this.gameSession = gameSession;
	}

	public void exec(GameMechanics gameMechanics)
	{
		gameMechanics.GetGameState(firstUserSessionId, firstUserId,
							secondUserSessionId, secondUserId,gameSession);
	}
}
