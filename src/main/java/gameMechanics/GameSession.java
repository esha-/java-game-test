package gameMechanics;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;



import modules.*;


public class GameSession implements Session
{
	private int idFirstUser;
	private int idSecondUser;

	private int idSessionOnFrontEndFirst;
	private int idSessionOnFrontEndSecond;

	private Map<Integer,Integer> sesIdAndClicks = new HashMap<Integer,Integer>();

	private int gameSessionId;

	private long gameBeginTime;

	private long TIME_FOR_GAME;

	private int amountOfClicks;

	private long lastUseOfSession;

	GameSession(int idFirstUser, int idSecondUser,int idSessionOnFrontEndFirst,
				int idSessionOnFrontEndSecond, int gameSessionId,int TIME_FOR_GAME)

	{
		lastUseOfSession = gameBeginTime = (new Date()).getTime();
		this.idFirstUser = idFirstUser;
		this.idSecondUser = idSecondUser;
		this.idSessionOnFrontEndFirst = idSessionOnFrontEndFirst;
		this.idSessionOnFrontEndSecond = idSessionOnFrontEndSecond;
		this.gameSessionId = gameSessionId;

		sesIdAndClicks.put(idSessionOnFrontEndFirst,0);
		sesIdAndClicks.put(idSessionOnFrontEndSecond,0);

		this.TIME_FOR_GAME = TIME_FOR_GAME;
	}

	public long getLastUseOfSession()
	{
		return lastUseOfSession;
	}

	public int getGameSessionId()
	{
		lastUseOfSession = (new Date()).getTime();
		return gameSessionId;
	}

	public long getTIME_FOR_GAME()
	{
		lastUseOfSession = (new Date()).getTime();
		return TIME_FOR_GAME;
	}

	public void addClickInMap(int sessionId)
	{
		int i =  sesIdAndClicks.get(sessionId);
		sesIdAndClicks.remove(sessionId);

		sesIdAndClicks.put(sessionId,++i);

		lastUseOfSession = (new Date()).getTime();

	}

	public int getClickBySessionId(int sessionId)
	{
		lastUseOfSession = (new Date()).getTime();
		return sesIdAndClicks.get(sessionId);
	}

	public int getAmountOfClicks()
	{
		return amountOfClicks;
	}

	public void addOneClick()
	{
		amountOfClicks++;
	}

	public long getGameBeginTime()
	{
		lastUseOfSession = (new Date()).getTime();
		return gameBeginTime;
	}
	public void setGameBeginTime()
	{
		gameBeginTime = (new Date()).getTime();
	}

	public int getIdFirstUser()
	{
		return idFirstUser;
	}

	public int getIdSecondUser()
	{
		return idSecondUser;
	}

	public int getIdSessionOnFrontEndFirst()
	{
		return idSessionOnFrontEndFirst;
	}

	public int getIdSessionOnFrontEndSecond()
	{
		return idSessionOnFrontEndSecond;
	}
	public int getSessionId()
	{
		return gameSessionId;
	}

}
